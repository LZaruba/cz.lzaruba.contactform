/**
 * This file is part of Demo Contact Form.
 *
 * Demo Contact Form is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Demo Contact Form is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Demo Contact Form.  If not, see <https://www.gnu.org/licenses/>
 */
package cz.lzaruba.contactform.backend.service.impl;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolationException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import cz.lzaruba.contactform.backend.config.ValidationTestConfig;
import cz.lzaruba.contactform.backend.model.ContactFormRequest;
import cz.lzaruba.contactform.backend.model.RequestKind;
import cz.lzaruba.contactform.backend.repo.ContactFormRequestRepository;
import cz.lzaruba.contactform.backend.repo.RequestKindRepository;
import cz.lzaruba.contactform.backend.service.ContactFormService;

/**
 * @author Lukas Zaruba, lukas.zaruba@gmail.com, 2019
 */
@ExtendWith(SpringExtension.class)
public class ContactFormServiceImplTest {

	@TestConfiguration
	@Import({ ValidationTestConfig.class })
	static class Config {

		@Autowired
		private ApplicationContext ctx;

		@Bean
		public ContactFormService contactFormService() {
			return ctx.getAutowireCapableBeanFactory().createBean(ContactFormServiceImpl.class);
		}

	}

	@Autowired
	private ContactFormService contactFormService;

	@MockBean
	private ContactFormRequestRepository requestRepo;

	@MockBean
	private RequestKindRepository requestKindRepo;
	
	@BeforeEach
	void before() {
		when(requestKindRepo.existsById("id1")).thenReturn(true);
	}

	@Test
	void saveNull() throws Exception {
		assertThrows(ConstraintViolationException.class, () -> contactFormService.saveRequest(null));
	}

	@Test
	void saveInvalid() throws Exception {
		assertThrows(ConstraintViolationException.class, () -> contactFormService.saveRequest(new ContactFormRequest()));
	}
	
	@Test
	void saveMissingKind() throws Exception {
		IllegalArgumentException thrown = assertThrows(IllegalArgumentException.class, () -> contactFormService.saveRequest(req().setRequestKind("otherId")));
		assertThat(thrown.getMessage(), containsString("otherId"));
	}

	@Test
	void save() throws Exception {
		when(requestRepo.insert(Mockito.any(ContactFormRequest.class))).thenReturn(new ContactFormRequest().setId("resultId"));
		assertThat(contactFormService.saveRequest(req()), is("resultId"));
		verify(requestRepo, times(1)).insert(req());
	}

	@Test
	void getKindsEmpty() throws Exception {
		assertThat(contactFormService.getKinds().size(), is(0));
	}

	@Test
	void getKinds() throws Exception {
		when(requestKindRepo.findByOrderByOrderAsc()).thenReturn(List.of(
				new RequestKind().setId("id1").setName("name1").setOrder(0),
				new RequestKind().setId("id2").setName("name2").setOrder(1)));
		assertThat(contactFormService.getKinds().stream().map(RequestKind::getId).collect(Collectors.toList()), is(List.of("id1", "id2")));
	}

	private ContactFormRequest req() {
		return new ContactFormRequest()
				.setRequestKind("id1")
				.setPolicyNumber("number")
				.setFirstName("firstName")
				.setLastName("lastName")
				.setContent("content");
	}

}
