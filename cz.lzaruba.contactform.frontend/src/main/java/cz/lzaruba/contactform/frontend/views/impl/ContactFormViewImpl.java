/**
 * This file is part of Demo Contact Form.
 *
 * Demo Contact Form is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Demo Contact Form is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Demo Contact Form.  If not, see <https://www.gnu.org/licenses/>
 */
package cz.lzaruba.contactform.frontend.views.impl;

import java.util.List;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.vaadin.firitin.components.button.VButton;
import org.vaadin.firitin.components.combobox.VComboBox;
import org.vaadin.firitin.components.html.VSpan;
import org.vaadin.firitin.components.orderedlayout.VVerticalLayout;
import org.vaadin.firitin.components.textfield.VTextArea;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.formlayout.FormLayout.ResponsiveStep;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.spring.annotation.SpringComponent;

import cz.lzaruba.contactform.frontend.dto.ContactFormRequestDTO;
import cz.lzaruba.contactform.frontend.dto.ContactFormResponseType;
import cz.lzaruba.contactform.frontend.dto.RequestKindDTO;
import cz.lzaruba.contactform.frontend.presenter.ContactFormPresenter;
import cz.lzaruba.contactform.frontend.views.ContactFormView;

/**
 * @author Lukas Zaruba, lukas.zaruba@gmail.com, 2019
 */
@SuppressWarnings("serial")
@SpringComponent
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ContactFormViewImpl extends VVerticalLayout implements ContactFormView {

	private ContactFormPresenter presenter;
	private List<RequestKindDTO> kinds;
	private VSpan message;
	private Binder<ContactFormRequestDTO> binder;
	
	// fields
	private ComboBox<RequestKindDTO> requestKind;
	private TextField policyNumber;
	private TextField firstName;
	private TextField lastName;
	private TextArea content;

	@Override
	public void init(ContactFormPresenter presenter) {
		this.presenter = presenter;
		this.kinds = presenter.getRequestKinds();
		createContent();
	}

	private void createContent() {
		setDefaultHorizontalComponentAlignment(Alignment.CENTER);
		getElement().getStyle().set("max-width", "25em");
		createHeader();
		createMessage();
		createInnerContent();
	}

	private void createHeader() {
		add(new H1(getTranslation("header")));
	}

	private void createMessage() {
		message = new VSpan().withVisible(false);
		add(message);
	}

	private void createInnerContent() {
		initFields();
		bindFields();
		createForm();
		createSubmit();
	}

	private void initFields() {
		requestKind = new VComboBox<>(getTranslation("form.requestKind"));
		requestKind.setPreventInvalidInput(true);
		requestKind.setItemLabelGenerator(RequestKindDTO::getName);
		requestKind.setItems(kinds);
		requestKind.setAutofocus(true);
	
		policyNumber = new TextField(getTranslation("form.policyNumber"));
	
		firstName = new TextField(getTranslation("form.firstName"));
	
		lastName = new TextField(getTranslation("form.lastName"));
	
		content = new VTextArea(getTranslation("form.content")).withHeight("10em");
	}

	private void bindFields() {
		binder = new BeanValidationBinder<>(ContactFormRequestDTO.class);
		binder.forField(requestKind)
			.asRequired(getTranslation("validation.required"))
			.bind(dto -> kinds.stream().filter(k -> k.getId().equals(dto.getRequestKind())).findFirst().orElse(null), (dto, k) -> dto.setRequestKind(k.getId()));
		binder.forField(policyNumber).bind("policyNumber");
		binder.forField(firstName).bind("firstName");
		binder.forField(lastName).bind("lastName");
		binder.forField(content).bind("content");
	}

	private void createForm() {
		FormLayout form = new FormLayout(requestKind, policyNumber, firstName, lastName, content);
		form.setResponsiveSteps(new ResponsiveStep("0", 1));
		add(form);
	}

	private void createSubmit() {
		Button submit = new VButton(getTranslation("submit"), VaadinIcon.CHECK.create())
				.withClickListener(e -> {
					if (!binder.validate().isOk()) return;
					presenter.requestFormSubmit();
				});
		add(submit);
		setHorizontalComponentAlignment(Alignment.END, submit);
	}

	@Override
	public Component getComponent() {
		return this;
	}

	@Override
	public void showMessage(ContactFormResponseType type) {
		message.withVisible(true).withText(getMessageContent(type));
		message.getElement().getStyle().set("background-color", getMessageColor(type));
	}

	private String getMessageContent(ContactFormResponseType type) {
		return getTranslation("result." + type.name().toLowerCase());
	}

	private String getMessageColor(ContactFormResponseType type) {
		if (type == ContactFormResponseType.OK) {
			return "var(--lumo-success-color-10pct)";
		}
		return "var(--lumo-error-color-10pct)";
	}

	@Override
	public void bindData(ContactFormRequestDTO dto) {
		binder.setBean(dto);
	}

}
