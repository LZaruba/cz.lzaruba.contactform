/**
 * This file is part of Demo Contact Form.
 *
 * Demo Contact Form is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Demo Contact Form is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Demo Contact Form.  If not, see <https://www.gnu.org/licenses/>
 */
package cz.lzaruba.contactform.frontend.presenter.impl;

import static java.util.Collections.unmodifiableList;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;

import cz.lzaruba.contactform.frontend.dto.ContactFormRequestDTO;
import cz.lzaruba.contactform.frontend.dto.ContactFormResponseType;
import cz.lzaruba.contactform.frontend.dto.RequestKindDTO;
import cz.lzaruba.contactform.frontend.presenter.ContactFormPresenter;
import cz.lzaruba.contactform.frontend.service.ContactFormService;
import cz.lzaruba.contactform.frontend.views.ContactFormView;

/**
 * @author Lukas Zaruba, lukas.zaruba@gmail.com, 2019
 */
@UIScope
@SpringComponent
public class ContactFormPresenterImpl implements ContactFormPresenter {

	@Autowired
	private ContactFormService service;

	@Autowired
	private ContactFormView view;

	private ContactFormRequestDTO dto;

	private List<RequestKindDTO> kinds;

	@PostConstruct
	public void postConstruct() {
		this.kinds = service.getKinds();
		view.init(this);
		refreshData();
	}

	private void refreshData() {
		this.dto = new ContactFormRequestDTO();
		view.bindData(dto);
	}

	@Override
	public Component getView() {
		return view.getComponent();
	}

	@Override
	public void requestFormSubmit() {
		ContactFormResponseType result = service.sendRequest(dto);
		view.showMessage(result);
		if (result == ContactFormResponseType.OK) {
			refreshData();
		}
	}

	@Override
	public List<RequestKindDTO> getRequestKinds() {
		return unmodifiableList(kinds);
	}

}
