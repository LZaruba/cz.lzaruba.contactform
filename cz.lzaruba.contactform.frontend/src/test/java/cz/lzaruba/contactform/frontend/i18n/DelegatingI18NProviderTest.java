/**
 * This file is part of Demo Contact Form.
 *
 * Demo Contact Form is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Demo Contact Form is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Demo Contact Form.  If not, see <https://www.gnu.org/licenses/>
 */
package cz.lzaruba.contactform.frontend.i18n;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Locale;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import cz.lzaruba.contactform.frontend.config.MessagesConfig;

/**
 * @author Lukas Zaruba, lukas.zaruba@gmail.com, 2019
 */
@ExtendWith(SpringExtension.class)
public class DelegatingI18NProviderTest {

	@TestConfiguration
	@Import({ MessagesConfig.class })
	static class TestConfig {

		@Autowired
		private ApplicationContext ctx;

		@Bean
		public DelegatingI18NProvider delegatingI18NProvider() {
			return ctx.getAutowireCapableBeanFactory().createBean(DelegatingI18NProvider.class);
		}

	}

	@Autowired
	private DelegatingI18NProvider p;

	@Test
	void loadKeyEN() throws Exception {
		assertEquals("Test 1 en", p.getTranslation("testBasic", new Locale("en")));
	}

	@Test
	void loadKeyCS() throws Exception {
		assertEquals("Test 1 cs", p.getTranslation("testBasic", new Locale("cs")));
	}

	@Test
	void loadKeyCSVariant() throws Exception {
		assertEquals("Test 1 cs", p.getTranslation("testBasic", new Locale("cs", "CZ")));
	}

}
