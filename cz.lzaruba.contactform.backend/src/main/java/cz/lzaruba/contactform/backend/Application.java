/**
 * This file is part of Demo Contact Form.
 *
 * Demo Contact Form is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Demo Contact Form is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Demo Contact Form. If not, see <https://www.gnu.org/licenses/>
 */
package cz.lzaruba.contactform.backend;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import cz.lzaruba.contactform.backend.model.RequestKind;
import cz.lzaruba.contactform.backend.repo.RequestKindRepository;

/**
 * @author Lukas Zaruba, lukas.zaruba@gmail.com, 2019
 */
@SpringBootApplication
public class Application implements CommandLineRunner {

	@Autowired
	private RequestKindRepository requestKindsRepo;

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		if (requestKindsRepo.count() != 0) return;
		requestKindsRepo.insert(List.of(
				new RequestKind().setName("Contract Adjustment").setOrder(0),
				new RequestKind().setName("Damage Case").setOrder(1),
				new RequestKind().setName("Complaint").setOrder(2)));
	}

}
