/**
 * This file is part of Demo Contact Form.
 *
 * Demo Contact Form is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Demo Contact Form is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Demo Contact Form.  If not, see <https://www.gnu.org/licenses/>
 */
package cz.lzaruba.contactform.backend.rest;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.ObjectMapper;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import cz.lzaruba.contactform.backend.model.ContactFormRequest;
import cz.lzaruba.contactform.backend.model.RequestKind;
import cz.lzaruba.contactform.backend.repo.RequestKindRepository;
import cz.lzaruba.contactform.backend.service.ContactFormService;

/**
 * @author Lukas Zaruba, lukas.zaruba@gmail.com, 2019
 */
@ExtendWith(SpringExtension.class)
@WebMvcTest(ContactFormRestController.class)
public class ContactFormRestControllerTest {

	@Autowired
    private MockMvc mvc;

	@Autowired
	protected ObjectMapper objectMapper;

    @MockBean
    private ContactFormService contactFormService;

    // to prevent failure when loading Application Context because of reference from CLR
    @MockBean
    private RequestKindRepository requestKindRepository;

    @Test
	void getKindsEmpty() throws Exception {
    	when(contactFormService.getKinds()).thenReturn(List.of());
		mvc.perform(get("/api/v1/contact-form/kinds"))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$", empty()));
	}

    @Test
	void getKinds() throws Exception {
    	when(contactFormService.getKinds()).thenReturn(List.of(new RequestKind().setId("id1").setName("name1"), new RequestKind().setId("id2").setName("name2")));
		mvc.perform(get("/api/v1/contact-form/kinds"))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.[1].id", is("id2")));
	}

    @Test
	void saveInvalid() throws Exception {
    	mvc.perform(post("/api/v1/contact-form").contentType(MediaType.APPLICATION_JSON_UTF8).content(objectMapper.writeValueAsString(new ContactFormRequest())))
			.andExpect(status().isBadRequest());
	}

    @Test
  	void save() throws Exception {
    	when(contactFormService.saveRequest(req())).thenReturn("resultId");
      	mvc.perform(post("/api/v1/contact-form").contentType(MediaType.APPLICATION_JSON_UTF8).content(objectMapper.writeValueAsString(req())))
  			.andExpect(status().isCreated())
  			.andExpect(jsonPath("$", is("resultId")))
  			.andExpect(MockMvcResultMatchers.header().string(HttpHeaders.LOCATION, containsString("/api/v1/contact-form/resultId")));
      	verify(contactFormService, times(1)).saveRequest(req());
  	}

	private ContactFormRequest req() {
		return new ContactFormRequest()
				.setRequestKind("id1")
				.setPolicyNumber("policy")
				.setFirstName("firstName")
				.setLastName("lastName")
				.setContent("content");
	}

}
