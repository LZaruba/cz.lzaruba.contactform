/**
 * This file is part of Demo Contact Form.
 *
 * Demo Contact Form is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Demo Contact Form is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Demo Contact Form.  If not, see <https://www.gnu.org/licenses/>
 */
package cz.lzaruba.contactform.backend.model;

import java.time.LocalDateTime;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import cz.lzaruba.contactform.common.validation.LettersOnlyConstraint;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * @author Lukas Zaruba, lukas.zaruba@gmail.com, 2019
 */
@Setter @Getter @ToString @EqualsAndHashCode
@Accessors(chain = true)
@Document(collection = "requests")
public class ContactFormRequest {
	
	@Id
	private String id;

	/**
	 * Id of the Request Kind
	 */
	@NotEmpty
	private String requestKind;

	@NotEmpty
	@Pattern(regexp = "^[a-zA-Z0-9]+$")
	private String policyNumber;

	@NotEmpty
	@LettersOnlyConstraint
	private String firstName;

	@NotEmpty
	@LettersOnlyConstraint
	private String lastName;

	@NotEmpty
	@Length(min = 1, max = 5000)
	private String content;

	@CreatedDate
	private LocalDateTime createdDate;

}
