/**
 * This file is part of Demo Contact Form.
 *
 * Demo Contact Form is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Demo Contact Form is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Demo Contact Form.  If not, see <https://www.gnu.org/licenses/>
 */
package cz.lzaruba.contactform.frontend.presenter.impl;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.vaadin.firitin.components.html.VLabel;

import com.vaadin.flow.component.Component;

import static java.util.stream.Collectors.toList;

import cz.lzaruba.contactform.frontend.dto.ContactFormResponseType;
import cz.lzaruba.contactform.frontend.dto.RequestKindDTO;
import cz.lzaruba.contactform.frontend.presenter.ContactFormPresenter;
import cz.lzaruba.contactform.frontend.service.ContactFormService;
import cz.lzaruba.contactform.frontend.views.ContactFormView;

/**
 * @author Lukas Zaruba, lukas.zaruba@gmail.com, 2019
 */
@ExtendWith(SpringExtension.class)
public class ContactFormPresenterImplTest {

	@TestConfiguration
	static class TestConfig {

		@Autowired
		private ApplicationContext ctx;

		@Bean
		public ContactFormPresenter contactFormPresenter() {
			return ctx.getAutowireCapableBeanFactory().createBean(ContactFormPresenterImpl.class);
		}

		@Bean
		public ContactFormService contactFormService() {
			ContactFormService s = Mockito.mock(ContactFormService.class);
			when(s.getKinds()).thenReturn(List.of(new RequestKindDTO().setId("id1").setName("n1"), new RequestKindDTO().setId("id2").setName("n2")));
			return s;
		}

	}

	@Autowired
	private ContactFormPresenter p;

	@Autowired
	private ContactFormService s;

	@MockBean
	private ContactFormView view;

	@BeforeEach
	void before() {
		when(view.getComponent()).thenReturn(new VLabel().withId("viewId"));
	}

	@Test
	void viewWiring() throws Exception {
		Component v = p.getView();
		assertThat(v.getId().get(), is("viewId"));
	}

	@Test
	void kinds() throws Exception {
		assertThat(p.getRequestKinds().stream().map(RequestKindDTO::getId).collect(toList()), is(List.of("id1", "id2")));
	}

	@Test
	void submitOk() throws Exception {
		when(s.sendRequest(Mockito.any())).thenReturn(ContactFormResponseType.OK);
		p.requestFormSubmit();
		verify(s, times(1)).sendRequest(Mockito.any());
		verify(view, times(1)).showMessage(ContactFormResponseType.OK);
		verify(view, atLeastOnce()).bindData(Mockito.any());
	}

	@Test
	void submitError() throws Exception {
		when(s.sendRequest(Mockito.any())).thenReturn(ContactFormResponseType.ERROR);
		p.requestFormSubmit();
		verify(view, times(1)).showMessage(ContactFormResponseType.ERROR);
		verify(view, times(0)).bindData(Mockito.any());
	}

}
