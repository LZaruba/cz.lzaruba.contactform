# The Contact Form

The contact form demo application

## Goal
The goal of this project is to create a web contact form that loads options and saves input into a persistence layer.

## Solution
The application is a showcase of microservices architecture based on Spring and Spring Boot. MongoDB represents the persistence layer and frontend is a web application with Vaadin Flow framework. The service layer microservice publishes REST endpoint for frontend and possibly for API access.

The result of the build is a couple of Docker images that are designed to be deployed into Docker Compose.

## Scope
Designing fully-blown microservice runtime is out of the scope of this project. Service discovery, high availability with client-side load-balancing, common configuration solution, monitoring, and security are thus not implemented. Also, Vaadin frontend is not configured for full production.

Configuration and optimization of the MongoDB are also out of the scope of this project. A single instance from basic Docker images is used.

## Future Enhancements
We can take the whole stack a step further and make it reactive. MongoDB provides asynchronous drivers, Spring Data MongoDB supports reactive repositories, and Vaadin UI supports reactivity by using Vaadin Push to access and modify UI with results in an asynchronous way. And of course, Spring gives us support for non-blocking REST calls between WebClient and Router backend.

The Vaadin frontend deserves enhancements in many areas. It can have a better offline experience, it can be minimized, transpiled and packed for production use, the advanced configuration is minimal - no meta information, no favicon, no push configuration, and there are no automated UI tests.

Maven build doesn't check for CVEs.

## How to build the application
Run the following command to build jars and deploy the Docker images in the parent folder.

`mvn package`

Please note that both images are available in public Docker Hub repositories under names `lzaruba/contact-form-frontend` and `lzaruba/contact-form-backend` and there is no need to build them locally.

## How to run the application
To run the application, navigate to `cz.lzaruba.contactform.deployment` subfolder and execute the following command.

`docker-compose up`

MongoDB is started as one of the containers, and there is no need to run a Mongo instance locally. Access to the Mongo instance is forwarded on port 27018.

## How to access the application
The application is available via browser at http://localhost:8080.





