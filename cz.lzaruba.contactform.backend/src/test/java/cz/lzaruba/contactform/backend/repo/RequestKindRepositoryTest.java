/**
 * This file is part of Demo Contact Form.
 *
 * Demo Contact Form is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Demo Contact Form is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Demo Contact Form.  If not, see <https://www.gnu.org/licenses/>
 */
package cz.lzaruba.contactform.backend.repo;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import cz.lzaruba.contactform.backend.MongoTest;
import cz.lzaruba.contactform.backend.model.RequestKind;

/**
 * @author Lukas Zaruba, lukas.zaruba@gmail.com, 2019
 */
public class RequestKindRepositoryTest extends MongoTest {

	@Autowired
	private RequestKindRepository requestKindRepo;

	@Test
	void emptyData() throws Exception {
		assertThat(requestKindRepo.findByOrderByOrderAsc().size(), is(0));
	}

	@Test
	void sorting() throws Exception {
		ops.insert(new RequestKind().setName("n1").setOrder(1));
		ops.insert(new RequestKind().setName("n2").setOrder(0));
		ops.insert(new RequestKind().setName("n3").setOrder(2));
		assertThat(requestKindRepo.findByOrderByOrderAsc().size(), is(3));
		assertThat(requestKindRepo.findByOrderByOrderAsc().stream().map(RequestKind::getName).collect(Collectors.toList()), is(List.of("n2", "n1", "n3")));
	}


}
