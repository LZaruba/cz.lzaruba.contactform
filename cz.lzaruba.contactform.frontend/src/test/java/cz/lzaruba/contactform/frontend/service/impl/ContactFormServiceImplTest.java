/**
 * This file is part of Demo Contact Form.
 *
 * Demo Contact Form is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Demo Contact Form is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Demo Contact Form.  If not, see <https://www.gnu.org/licenses/>
 */
package cz.lzaruba.contactform.frontend.service.impl;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolationException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestTemplate;

import cz.lzaruba.contactform.frontend.config.ConfigProperties;
import cz.lzaruba.contactform.frontend.config.ValidationTestConfig;
import cz.lzaruba.contactform.frontend.dto.ContactFormRequestDTO;
import cz.lzaruba.contactform.frontend.dto.ContactFormResponseType;
import cz.lzaruba.contactform.frontend.dto.RequestKindDTO;
import cz.lzaruba.contactform.frontend.service.ContactFormService;

/**
 * @author Lukas Zaruba, lukas.zaruba@gmail.com, 2019
 */
@ExtendWith(SpringExtension.class)
public class ContactFormServiceImplTest {

	@TestConfiguration
	@Import({ ValidationTestConfig.class, ConfigProperties.class })
	static class Config {

		@Autowired
		private ApplicationContext ctx;

		@Bean
		public ContactFormService contactFormService() {
			return ctx.getAutowireCapableBeanFactory().createBean(ContactFormServiceImpl.class);
		}

	}

	@Autowired
	private ContactFormService s;

	@MockBean
	private RestTemplate rt;

	@Test
	void sendRequestNull() throws Exception {
		assertThrows(ConstraintViolationException.class, () -> s.sendRequest(null));
	}

	@Test
	void sendRequestInvalid() throws Exception {
		assertThrows(ConstraintViolationException.class, () -> s.sendRequest(new ContactFormRequestDTO()));
	}

	@Test
	void sendRequestError() throws Exception {
		when(rt.postForEntity("http://localhost:8081/api/v1/contact-form", dto(), Void.class)).thenThrow(RuntimeException.class);
		assertThat(s.sendRequest(dto()), is(ContactFormResponseType.ERROR));
	}

	@Test
	void sendRequestNon201() throws Exception {
		when(rt.postForEntity("http://localhost:8081/api/v1/contact-form", dto(), Void.class)).thenReturn(ResponseEntity.status(HttpStatus.FOUND).build());
		assertThat(s.sendRequest(dto()), is(ContactFormResponseType.ERROR));
	}

	@Test
	void sendRequest() throws Exception {
		when(rt.postForEntity("http://localhost:8081/api/v1/contact-form", dto(), Void.class)).thenReturn(ResponseEntity.status(HttpStatus.CREATED).build());
		assertThat(s.sendRequest(dto()), is(ContactFormResponseType.OK));
	}

	private ContactFormRequestDTO dto() {
		return new ContactFormRequestDTO("id1", "policyNumber", "firstName", "lastName", "content");
	}

	@SuppressWarnings("unchecked")
	@Test
	void getKinds() throws Exception {
		when(rt.exchange(Mockito.eq("http://localhost:8081/api/v1/contact-form/kinds"), Mockito.eq(HttpMethod.GET), Mockito.isNull(), Mockito.any(ParameterizedTypeReference.class)))
				.thenReturn(ResponseEntity.ok(List.of(new RequestKindDTO().setId("id1").setName("name1"), new RequestKindDTO().setId("id2").setName("name2"))));
		assertThat(s.getKinds().stream().map(RequestKindDTO::getId).collect(Collectors.toList()), is(List.of("id1", "id2")));
	}

	@SuppressWarnings("unchecked")
	@Test
	void getKindsNon200() throws Exception {
		when(rt.exchange(Mockito.eq("http://localhost:8081/api/v1/contact-form/kinds"), Mockito.eq(HttpMethod.GET), Mockito.isNull(), Mockito.any(ParameterizedTypeReference.class)))
				.thenReturn(ResponseEntity.badRequest().build());
		assertThat(s.getKinds().size(), is(0));
	}

	@SuppressWarnings("unchecked")
	@Test
	void getKindsError() throws Exception {
		when(rt.exchange(Mockito.eq("http://localhost:8081/api/v1/contact-form/kinds"), Mockito.eq(HttpMethod.GET), Mockito.isNull(), Mockito.any(ParameterizedTypeReference.class))).thenThrow(RuntimeException.class);
		assertThat(s.getKinds().size(), is(0));
	}

}
