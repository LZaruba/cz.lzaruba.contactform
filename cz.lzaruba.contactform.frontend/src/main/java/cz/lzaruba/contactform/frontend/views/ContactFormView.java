/**
 * This file is part of Demo Contact Form.
 *
 * Demo Contact Form is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Demo Contact Form is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Demo Contact Form.  If not, see <https://www.gnu.org/licenses/>
 */
package cz.lzaruba.contactform.frontend.views;

import com.vaadin.flow.component.Component;

import cz.lzaruba.contactform.frontend.dto.ContactFormRequestDTO;
import cz.lzaruba.contactform.frontend.dto.ContactFormResponseType;
import cz.lzaruba.contactform.frontend.presenter.ContactFormPresenter;

/**
 * @author Lukas Zaruba, lukas.zaruba@gmail.com, 2019
 */
public interface ContactFormView {

	/**
	 * Invokes initialization with given presenter, creating content
	 */
	void init(ContactFormPresenter presenter);

	/**
	 * @return view represented as a Vaadin flow component
	 */
	Component getComponent();

	/**
	 * Displays message above the form in style based on the given type
	 */
	void showMessage(ContactFormResponseType type);

	/**
	 * Binds given DTO to the form
	 */
	void bindData(ContactFormRequestDTO dto);

}
