/**
 * This file is part of Demo Contact Form.
 *
 * Demo Contact Form is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Demo Contact Form is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Demo Contact Form.  If not, see <https://www.gnu.org/licenses/>
 */
package cz.lzaruba.contactform.backend.rest;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import cz.lzaruba.contactform.backend.model.ContactFormRequest;
import cz.lzaruba.contactform.backend.model.RequestKind;
import cz.lzaruba.contactform.backend.service.ContactFormService;

/**
 * @author Lukas Zaruba, lukas.zaruba@gmail.com, 2019
 */
@RestController
@RequestMapping("/api/v1/contact-form")
public class ContactFormRestController {

	@Autowired
	private ContactFormService contactFormService;

	/**
	 * Loads available Request Kinds from persistence and presents them in appropriate order
	 */
	@GetMapping(value = "/kinds")
	public @NotNull List<RequestKind> getKinds() {
		return contactFormService.getKinds();
	}

	/**
	 * Saves given request into the persistence returning 201 Created response on success
	 */
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<String> saveRequest(@RequestBody @Valid ContactFormRequest request) {
		String id = contactFormService.saveRequest(request);
		URI location = ServletUriComponentsBuilder
				.fromCurrentRequest().path("/{id}")
				.buildAndExpand(id).toUri();
		return ResponseEntity.created(location).body(id);
	}

}
