/**
 * This file is part of Demo Contact Form.
 *
 * Demo Contact Form is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Demo Contact Form is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Demo Contact Form.  If not, see <https://www.gnu.org/licenses/>
 */
package cz.lzaruba.contactform.frontend.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;

import cz.lzaruba.contactform.common.validation.LettersOnlyConstraint;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Lukas Zaruba, lukas.zaruba@gmail.com, 2019
 */
@Setter @Getter @ToString @EqualsAndHashCode
@AllArgsConstructor @NoArgsConstructor
public class ContactFormRequestDTO {

	/**
	 * Id of the Request Kind
	 */
	@NotEmpty
	private String requestKind;

	@NotEmpty
	@Pattern(regexp = "^[a-zA-Z0-9]+$", message = "{cz.lzaruba.contactform.validation.alphanum}") 
	private String policyNumber;

	@NotEmpty
	@LettersOnlyConstraint(message = "{cz.lzaruba.contactform.validation.lettersonly}")
	private String firstName;

	@NotEmpty
	@LettersOnlyConstraint(message = "{cz.lzaruba.contactform.validation.lettersonly}")
	private String lastName;

	@NotEmpty
	@Length(min = 1, max = 5000)
	private String content;

}
