/**
 * This file is part of Demo Contact Form.
 *
 * Demo Contact Form is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Demo Contact Form is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Demo Contact Form.  If not, see <https://www.gnu.org/licenses/>
 */
package cz.lzaruba.contactform.backend.model;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;

import cz.lzaruba.contactform.backend.MongoTest;
import cz.lzaruba.contactform.backend.config.MongoConfig;
import cz.lzaruba.contactform.backend.repo.ContactFormRequestRepository;

/**
 * @author Lukas Zaruba, lukas.zaruba@gmail.com, 2019
 */
@Import(MongoConfig.class)
public class ContactFormRequestTest extends MongoTest {

	@Autowired
	private ContactFormRequestRepository requestRepo;

	@Test
	void audit() throws Exception {
		requestRepo.save(new ContactFormRequest().setRequestKind("kind").setPolicyNumber("policyNumber").setFirstName("firstName")
				.setLastName("lastName").setContent("content"));
		assertThat(requestRepo.findAll().get(0).getCreatedDate().getDayOfMonth(), is(LocalDate.now().getDayOfMonth()));
	}

}
