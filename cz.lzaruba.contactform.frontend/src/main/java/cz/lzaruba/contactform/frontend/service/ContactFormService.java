/**
 * This file is part of Demo Contact Form.
 *
 * Demo Contact Form is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Demo Contact Form is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Demo Contact Form.  If not, see <https://www.gnu.org/licenses/>
 */
package cz.lzaruba.contactform.frontend.service;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import cz.lzaruba.contactform.frontend.dto.ContactFormRequestDTO;
import cz.lzaruba.contactform.frontend.dto.ContactFormResponseType;
import cz.lzaruba.contactform.frontend.dto.RequestKindDTO;

/**
 * @author Lukas Zaruba, lukas.zaruba@gmail.com, 2019
 */
@Validated
public interface ContactFormService {

	/**
	 * Sends given request to the backend service
	 * @return {@link ContactFormResponseType#OK} or {@link ContactFormResponseType#ERROR} based on result from backend
	 */
	@NotNull ContactFormResponseType sendRequest(@NotNull @Valid ContactFormRequestDTO dto);

	/**
	 * Loads list of available request kinds from backend
	 */
	List<RequestKindDTO> getKinds();

}
