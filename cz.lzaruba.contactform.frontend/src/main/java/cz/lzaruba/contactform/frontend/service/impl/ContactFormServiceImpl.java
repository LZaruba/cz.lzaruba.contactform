/**
 * This file is part of Demo Contact Form.
 *
 * Demo Contact Form is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Demo Contact Form is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Demo Contact Form.  If not, see <https://www.gnu.org/licenses/>
 */
package cz.lzaruba.contactform.frontend.service.impl;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import cz.lzaruba.contactform.frontend.config.ConfigProperties;
import cz.lzaruba.contactform.frontend.dto.ContactFormRequestDTO;
import cz.lzaruba.contactform.frontend.dto.ContactFormResponseType;
import cz.lzaruba.contactform.frontend.dto.RequestKindDTO;
import cz.lzaruba.contactform.frontend.service.ContactFormService;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Lukas Zaruba, lukas.zaruba@gmail.com, 2019
 */
@Service
@Slf4j
public class ContactFormServiceImpl implements ContactFormService {

	@Autowired
	private RestTemplate rt;

	@Autowired
	private ConfigProperties cfg;

	@Override
	public @NotNull ContactFormResponseType sendRequest(@NotNull @Valid ContactFormRequestDTO dto) {
		try {
			ResponseEntity<Void> resp = rt.postForEntity(cfg.getBackendBaseUrl(), dto, Void.class);
			if (resp.getStatusCode() != HttpStatus.CREATED) {
				log.warn("Unexpected status while sending request: {}" + resp.getStatusCodeValue());
				return ContactFormResponseType.ERROR;
			}
			return ContactFormResponseType.OK;
		} catch (Exception e) {
			log.error("Error while sending request", e);
			return ContactFormResponseType.ERROR;
		}
	}

	@Override
	public List<RequestKindDTO> getKinds() {
		try {
			ResponseEntity<List<RequestKindDTO>> resp = rt.exchange(cfg.getBackendBaseUrl() + "/kinds", HttpMethod.GET, null, new ParameterizedTypeReference<List<RequestKindDTO>>() {});
			if (resp.getStatusCode() != HttpStatus.OK) {
				log.warn("Loading of available kinds returned unexpected response code: {}", resp.getStatusCodeValue());
				return List.of();
			}
			return resp.getBody();
		} catch (Exception e) {
			log.error("Error while loading available kinds", e);
			return List.of();
		}
	}

}
