/**
 * This file is part of Demo Contact Form.
 *
 * Demo Contact Form is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Demo Contact Form is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Demo Contact Form.  If not, see <https://www.gnu.org/licenses/>
 */
package cz.lzaruba.contactform.backend.service.impl;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cz.lzaruba.contactform.backend.model.ContactFormRequest;
import cz.lzaruba.contactform.backend.model.RequestKind;
import cz.lzaruba.contactform.backend.repo.ContactFormRequestRepository;
import cz.lzaruba.contactform.backend.repo.RequestKindRepository;
import cz.lzaruba.contactform.backend.service.ContactFormService;

/**
 * @author Lukas Zaruba, lukas.zaruba@gmail.com, 2019
 */
@Service
public class ContactFormServiceImpl implements ContactFormService {

	@Autowired
	private RequestKindRepository requestKindRepo;

	@Autowired
	private ContactFormRequestRepository requestRepo;

	@Override
	public @NotEmpty String saveRequest(@NotNull @Valid ContactFormRequest request) {
		if (!requestKindRepo.existsById(request.getRequestKind())) {
			throw new IllegalArgumentException("Invalid request kind: " + request.getRequestKind());
		}
		return requestRepo.insert(request).getId();
	}

	@Override
	public @NotNull List<RequestKind> getKinds() {
		return requestKindRepo.findByOrderByOrderAsc();
	}

}
