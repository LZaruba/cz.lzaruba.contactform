/**
 * This file is part of Demo Contact Form.
 *
 * Demo Contact Form is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Demo Contact Form is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Demo Contact Form.  If not, see <https://www.gnu.org/licenses/>
 */
package cz.lzaruba.contactform.frontend.presenter;

import java.util.List;

import com.vaadin.flow.component.Component;

import cz.lzaruba.contactform.frontend.dto.RequestKindDTO;

/**
 * @author Lukas Zaruba, lukas.zaruba@gmail.com, 2019
 */
public interface ContactFormPresenter {

	/**
	 * @return view connected to this presenter
	 */
	Component getView();

	/**
	 * Requests submitting of the form request
	 */
	void requestFormSubmit();

	/**
	 * @return list of available request kinds
	 */
	List<RequestKindDTO> getRequestKinds();

}
