/**
 * This file is part of Demo Contact Form.
 *
 * Demo Contact Form is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Demo Contact Form is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Demo Contact Form. If not, see <https://www.gnu.org/licenses/>
 */
package cz.lzaruba.contactform.backend;

import org.bson.Document;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 * @author Lukas Zaruba, lukas.zaruba@gmail.com, 2019
 */
@DataMongoTest
@ExtendWith(SpringExtension.class)
public abstract class MongoTest {

	@Autowired
	protected MongoOperations ops;

	@BeforeEach
	public void before() {
		ops.getCollectionNames().forEach(c -> {
			ops.getCollection(c).deleteMany(new Document());
		});
	}

}
