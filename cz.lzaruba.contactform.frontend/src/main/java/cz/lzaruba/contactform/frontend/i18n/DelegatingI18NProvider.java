/**
 * This file is part of Demo Contact Form.
 *
 * Demo Contact Form is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Demo Contact Form is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Demo Contact Form.  If not, see <https://www.gnu.org/licenses/>
 */
package cz.lzaruba.contactform.frontend.i18n;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import com.vaadin.flow.i18n.I18NProvider;

/**
 * @author Lukas Zaruba, lukas.zaruba@gmail.com, 2019
 */
@SuppressWarnings("serial")
@Component
public class DelegatingI18NProvider implements I18NProvider {

	@Autowired
	private MessageSource uiMessages;

	@Override
	public List<Locale> getProvidedLocales() {
		return List.of(Locale.ENGLISH, new Locale("cs"));
	}

	@Override
	public String getTranslation(String key, Locale locale, Object... params) {
		return uiMessages.getMessage(key, params, locale);
	}

}
