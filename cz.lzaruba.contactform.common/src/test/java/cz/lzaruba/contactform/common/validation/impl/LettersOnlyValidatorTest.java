/**
 * This file is part of Demo Contact Form.
 *
 * Demo Contact Form is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Demo Contact Form is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Demo Contact Form.  If not, see <https://www.gnu.org/licenses/>
 */
package cz.lzaruba.contactform.common.validation.impl;

import static org.junit.jupiter.api.Assertions.*;

import javax.validation.ConstraintValidatorContext;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 * @author Lukas Zaruba, lukas.zaruba@gmail.com, 2019
 */
@ExtendWith(SpringExtension.class)
public class LettersOnlyValidatorTest {
	
	@Mock
	private ConstraintValidatorContext ctx;
	
	@Test
	void nullValue() throws Exception {
		assertTrue(new LettersOnlyValidator().isValid(null, ctx));
	}
	
	@Test
	void emptyValue() throws Exception {
		assertTrue(new LettersOnlyValidator().isValid("", ctx));
	}
	
	@Test
	void emptyValue2() throws Exception {
		assertFalse(new LettersOnlyValidator().isValid(" ", ctx));
	}
	
	@Test
	void numericValue() throws Exception {
		assertFalse(new LettersOnlyValidator().isValid("123", ctx));
	}
	
	@Test
	void mixedValue() throws Exception {
		assertFalse(new LettersOnlyValidator().isValid("123adaf  ", ctx));
	}
	
	@Test
	void validValue1() throws Exception {
		assertTrue(new LettersOnlyValidator().isValid("abc", ctx));
	}
	
	@Test
	void validValue2() throws Exception {
		assertTrue(new LettersOnlyValidator().isValid("aBc", ctx));
	}
	
	@Test
	void validValue3() throws Exception {
		assertTrue(new LettersOnlyValidator().isValid("Lukáš", ctx));
	}

}
