/**
 * This file is part of Demo Contact Form.
 *
 * Demo Contact Form is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Demo Contact Form is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Demo Contact Form.  If not, see <https://www.gnu.org/licenses/>
 */
package cz.lzaruba.contactform.frontend.views.impl;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.page.Viewport;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.PWA;

import cz.lzaruba.contactform.frontend.presenter.ContactFormPresenter;

/**
 * @author Lukas Zaruba, lukas.zaruba@gmail.com, 2019
 */
@SuppressWarnings("serial")
@PWA(name = "Contact Form Demo Application", shortName = "ContactForm")
@Viewport("width=device-width")
@PageTitle("Contact Form")
@Route(value = "")
public class MainView extends VerticalLayout {

	public MainView(@Autowired ContactFormPresenter formPresenter) {
		createContent(formPresenter);
	}

	private void createContent(ContactFormPresenter formPresenter) {
		setSizeFull();
		Component view = formPresenter.getView();
		add(view);
		setHorizontalComponentAlignment(Alignment.CENTER, view);
	}

}
